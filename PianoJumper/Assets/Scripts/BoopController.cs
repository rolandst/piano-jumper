﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoopController : MonoBehaviour
{
    AudioSource boop;

    private void Start()
    {
        boop = GetComponent<AudioSource>();
    }

    public void PlayBoop()
    {
        boop.Play();
    }
}
