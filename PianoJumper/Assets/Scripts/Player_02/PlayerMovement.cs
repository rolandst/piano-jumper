﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using MidiJack;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public float force;
    public Animator animator;
    public GameObject DestroyedPt;

    GameController gc;
    ParticleSystem dust;
    Rigidbody2D rb;
    bool jump;
    bool isGrounded;
    UIController ui;

    // Start is called before the first frame update
    void Start()
    {
        GameVariables.LIVECOUNT = 3;
        GameVariables.NormalGameSpeed = true;
        gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        rb = GetComponent<Rigidbody2D>();
        dust = GetComponent<ParticleSystem>();
        ui = GameObject.FindGameObjectWithTag("UI").GetComponent<UIController>();

        StartCoroutine(ui.UpdateHearts());
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckPressedKeys() && isGrounded && GameVariables.ComboStack.Count > 0)
        //if (Input.GetKeyDown(KeyCode.Space) && isGrounded) // <-- For debugging purposes
        {
            jump = true;
        }
        else
        {
            jump = false;
        }

        if (GameVariables.GAMEPAUSED)
            dust.Pause();
        else if (isGrounded)
            dust.Play();
        else
            dust.Stop();        
    }

    private void FixedUpdate()
    {
        if(jump)
        {
            Jump();
        }
    }

    void Jump()
    {
        rb.AddForce(Vector2.up * force);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground")
        {
            isGrounded = true;
            animator.SetBool("IsGrounded", isGrounded);
        }
        else if (collision.collider.tag == "Obstacle" && GameVariables.LIVECOUNT > 0)
        {
            collision.collider.gameObject.SetActive(false);
            Destroy(collision.collider.gameObject);

            if (gc.GetObstacleCount() > 1)
                GameVariables.NormalGameSpeed = false;

            GameVariables.LIVECOUNT--;
            StartCoroutine(gc.SlowDownObstacles());
            StartCoroutine(ui.UpdateHearts());
        }        
        else if (collision.collider.tag == "Obstacle" && GameVariables.LIVECOUNT == 0)
        {
            Instantiate(DestroyedPt, transform.position, Quaternion.identity);
            ui.KeyBoard.SetActive(false);
            ui.IngameUI.SetActive(false);
            ui.GameLostUI.SetActive(true);
            StartCoroutine(ui.UpdateHearts());
            GameVariables.ComboNameStack.Clear();
            GameVariables.ComboStack.Clear();
            gc.UpdateComboText();
            GameVariables.GAMELOST = true;
            GameVariables.GAMEON = false;
            Destroy(gameObject);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Ground")
        {
            isGrounded = false;
            animator.SetBool("IsGrounded", isGrounded);
        }       
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "KeyChanger")
        {
            GameVariables.ComboStack.RemoveAt(0);
            GameVariables.ComboNameStack.RemoveAt(0);

            if (!GameVariables.NormalGameSpeed)
            {
                GameVariables.NormalGameSpeed = true;
                StartCoroutine(gc.SpeedUpObstacles());
            }

            gc.UpdateComboText();

            GameVariables.OverpassedObstacleCount++;
            gc.UpdateObstacleCount();
            gc.UpdateBPM();
        }
    }

    private bool CheckPressedKeys()
    {
        bool outcome = true;
        try
        {
            foreach (int i in GameVariables.ComboStack[0])
            {
                if (Convert.ToBoolean(MidiMaster.GetKey(i)))
                { continue; }
                else
                {
                    outcome = false;
                    break;
                }
            }
        }
        catch (ArgumentOutOfRangeException)
        {
            outcome = false;
        }

        return outcome;
    }
}