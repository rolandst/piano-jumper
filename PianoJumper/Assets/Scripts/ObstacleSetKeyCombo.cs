﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using UnityEngine;

public class ObstacleSetKeyCombo : MonoBehaviour
{
    void Start()
    {
        int maxRange = GameVariables.Chords.Count;
        int combo = Random.Range(0, maxRange);
        GameVariables.ComboStack.Add(GameVariables.Chords.ElementAt(combo).Value);
        GameVariables.ComboNameStack.Add(GameVariables.Chords.ElementAt(combo).Key);
        if (GameVariables.ComboStack.Count == 1)
        {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>().UpdateComboText();
        }
    }
}
