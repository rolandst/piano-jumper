﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public GameObject player;
    public Transform playerSpawnpoint;

    public void SpawnPlayer()
    {
        Instantiate(player, playerSpawnpoint.position, Quaternion.identity);
    }
}
