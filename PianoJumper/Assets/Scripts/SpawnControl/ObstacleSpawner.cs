﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public GameObject obstacle;
    public Transform spawnpoint;
    public Transform obstacleCollection;

    float spawnInterval;
    float timeToSpawn;

    // Update is called once per frame
    void Update()
    {
        if (GameVariables.GAMEON && !GameVariables.GAMEPAUSED && GameVariables.NormalGameSpeed)
        {
            timeToSpawn += Time.deltaTime;

            if (timeToSpawn >= spawnInterval)
            {
                Instantiate(obstacle, spawnpoint.position, Quaternion.identity, obstacleCollection);
                timeToSpawn = 0.0f;
            }           
        }
    }

    private void OnEnable()
    {
        SetSpawnInterval();
        timeToSpawn = 0.0f;
    }

    public void SetSpawnInterval()
    {
        spawnInterval = 60.0f / (float)GameVariables.BPM;
    }
}



            