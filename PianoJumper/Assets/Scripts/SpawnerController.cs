﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public GameObject obstacle;
    public GameObject sounds;
    public GameObject obstacleColletion;

    [HideInInspector]
    public float timeToSpawn;

    BoopController boop;
    float interval;

    // Start is called before the first frame update
    void Start()
    {
        boop = sounds.GetComponent<BoopController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameVariables.GAMEON && !GameVariables.GAMEPAUSED)
        {
            timeToSpawn += Time.deltaTime;

            if (timeToSpawn >= interval)
            { 
                Instantiate(obstacle, transform.position, Quaternion.identity, obstacleColletion.transform);
                boop.PlayBoop();
                timeToSpawn = 0.0f;
            }           
        }        
    }

    private void OnEnable()
    {
        InitInterval();
    }

    void InitInterval()
    {
        interval = 60.0f / GameVariables.BPM;
    }
}
