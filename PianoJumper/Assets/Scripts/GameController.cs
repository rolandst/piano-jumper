﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class GameController : MonoBehaviour
{
    public GameObject obstacleSpawner;
    public GameObject obstacleContainer;

    public Text ObstacleCountText;
    public Text ComboText;
    public Text BPMText;

    public List<GameObject> keys = new List<GameObject>();

    public void EnableObstacleSpawner()
    {
        obstacleSpawner.SetActive(true);
    }

    public void DisableObstacleSpawner()
    {
        obstacleSpawner.SetActive(false);
    }

    public IEnumerator DestrtoyAllObstacles()
    {
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacle");

        foreach (GameObject obstacle in obstacles)
        {
            Destroy(obstacle);
            yield return null;
        }
    }

    public void DestroyPlayer()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Destroy(player);
    }

    public void UpdateComboText()
    {
        if (GameVariables.ComboNameStack.Count > 0)
        {
            string text = GameVariables.ComboNameStack.ElementAt(0);
            string note = text.Split('_').First();
            string octave = text.Split('_').Last();

            ComboText.text = $"{note}<size=20>{octave}</size>";
        }
        else
        {
            ComboText.text = string.Empty;
        }

        //TODO Highlight keys

    }

    public IEnumerator SlowDownObstacles()
    {
        foreach (GameObject obstacle in GetObstacles())
        {
            obstacle.GetComponent<ObstacleMovement>().speed = 3.0f;

            yield return null;
        }
    }

    public IEnumerator SpeedUpObstacles()
    {
        foreach (GameObject obstacle in GetObstacles())
        {
            obstacle.GetComponent<ObstacleMovement>().speed = 10.0f;

            yield return null;
        }
    }

    public GameObject[] GetObstacles()
    {
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacle");

        return obstacles;
    }

    public int GetObstacleCount()
    {
        return obstacleContainer.transform.childCount;
    }

    public void UpdateObstacleCount()
    {
        ObstacleCountText.text = $"Obstacles: {GameVariables.OverpassedObstacleCount}";
    }

    public void UpdateBPM()
    {
        if (GameVariables.OverpassedObstacleCount % 10 == 0)
        {
            GameVariables.BPM++;
            BPMText.text = $"BPM: {GameVariables.BPM}";
        }
    }
}
