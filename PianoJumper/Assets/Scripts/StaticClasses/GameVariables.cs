﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameVariables : MonoBehaviour
{
    public static int BPM;
    public static int LIVECOUNT;
    public static int OverpassedObstacleCount;

    public static bool GAMEON = false;
    public static bool GAMEPAUSED = false;
    public static bool GAMELOST = false;

    public static bool NormalGameSpeed;

    public static List<string> ComboNameStack = new List<string>();
    public static List<int[]> ComboStack = new List<int[]>();

    public static Dictionary<string, int[]> Chords = new Dictionary<string, int[]>();
}
