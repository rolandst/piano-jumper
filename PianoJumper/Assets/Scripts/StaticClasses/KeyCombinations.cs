﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCombinations : MonoBehaviour
{
    private void Awake()
    {
        //GameVariables.Chords.Add("A_0", new int[] { 21 });
        //GameVariables.Chords.Add("A#_0", new int[] { 22 });
        //GameVariables.Chords.Add("B♭_0", new int[] { 22 });
        //GameVariables.Chords.Add("B_0", new int[] { 23 });

        //GameVariables.Chords.Add("C_1", new int[] { 24 });
        //GameVariables.Chords.Add("C#_1", new int[] { 25 });
        //GameVariables.Chords.Add("D♭_1", new int[] { 25 });
        //GameVariables.Chords.Add("D_1", new int[] { 26 });
        //GameVariables.Chords.Add("D#_1", new int[] { 27 });
        //GameVariables.Chords.Add("E♭_1", new int[] { 27 });
        //GameVariables.Chords.Add("E_1", new int[] { 28 });
        //GameVariables.Chords.Add("F_1", new int[] { 29 });
        //GameVariables.Chords.Add("F#_1", new int[] { 30 });
        //GameVariables.Chords.Add("G♭_1", new int[] { 30 });
        //GameVariables.Chords.Add("G_1", new int[] { 31 });
        //GameVariables.Chords.Add("G#_1", new int[] { 32 });
        //GameVariables.Chords.Add("A♭_1", new int[] { 32 });
        //GameVariables.Chords.Add("A_1", new int[] { 33 });
        //GameVariables.Chords.Add("A#_1", new int[] { 34 });
        //GameVariables.Chords.Add("B♭_1", new int[] { 34 });
        //GameVariables.Chords.Add("B_1", new int[] { 35 });

        //GameVariables.Chords.Add("C_2", new int[] { 36 });
        //GameVariables.Chords.Add("C#_2", new int[] { 37 });
        //GameVariables.Chords.Add("D♭_2", new int[] { 37 });
        //GameVariables.Chords.Add("D_2", new int[] { 38 });
        //GameVariables.Chords.Add("D#_2", new int[] { 39 });
        //GameVariables.Chords.Add("E♭_2", new int[] { 39 });
        //GameVariables.Chords.Add("E_2", new int[] { 40 });
        //GameVariables.Chords.Add("F_2", new int[] { 41 });
        //GameVariables.Chords.Add("F#_2", new int[] { 42 });
        //GameVariables.Chords.Add("G♭_2", new int[] { 42 });
        //GameVariables.Chords.Add("G_2", new int[] { 43 });
        //GameVariables.Chords.Add("G#_2", new int[] { 44 });
        //GameVariables.Chords.Add("A♭_2", new int[] { 44 });
        //GameVariables.Chords.Add("A_2", new int[] { 45 });
        //GameVariables.Chords.Add("A#_2", new int[] { 46 });
        //GameVariables.Chords.Add("B♭_2", new int[] { 46 });
        //GameVariables.Chords.Add("B_2", new int[] { 47 });

        //GameVariables.Chords.Add("C_3", new int[] { 48 });
        //GameVariables.Chords.Add("C#_3", new int[] { 49 });
        //GameVariables.Chords.Add("D♭_3", new int[] { 49 });
        //GameVariables.Chords.Add("D_3", new int[] { 50 });
        //GameVariables.Chords.Add("D#_3", new int[] { 51 });
        //GameVariables.Chords.Add("E♭_3", new int[] { 51 });
        //GameVariables.Chords.Add("E_3", new int[] { 52 });
        //GameVariables.Chords.Add("F_3", new int[] { 53 });
        //GameVariables.Chords.Add("F#_3", new int[] { 54 });
        //GameVariables.Chords.Add("G♭_3", new int[] { 54 });
        //GameVariables.Chords.Add("G_3", new int[] { 55 });
        //GameVariables.Chords.Add("G#_3", new int[] { 56 });
        //GameVariables.Chords.Add("A♭_3", new int[] { 56 });
        //GameVariables.Chords.Add("A_3", new int[] { 57 });
        //GameVariables.Chords.Add("A#_3", new int[] { 58 });
        //GameVariables.Chords.Add("B♭_3", new int[] { 58 });
        //GameVariables.Chords.Add("B_3", new int[] { 59 });

        GameVariables.Chords.Add("C_4", new int[] { 60 });
        //GameVariables.Chords.Add("C#_4", new int[] { 61 });
        //GameVariables.Chords.Add("D♭_4", new int[] { 61 });
        GameVariables.Chords.Add("D_4", new int[] { 62 });
        //GameVariables.Chords.Add("D#_4", new int[] { 63 });
        //GameVariables.Chords.Add("E♭_4", new int[] { 63 });
        GameVariables.Chords.Add("E_4", new int[] { 64 });
        GameVariables.Chords.Add("F_4", new int[] { 65 });
        //GameVariables.Chords.Add("F#_4", new int[] { 66 });
        //GameVariables.Chords.Add("G♭_4", new int[] { 66 });
        GameVariables.Chords.Add("G_4", new int[] { 67 });

        //TEST CHORDS
        GameVariables.Chords.Add("C_major", new int[] { 60, 64, 67 });
        GameVariables.Chords.Add("D_major", new int[] { 62, 66, 69 });
        GameVariables.Chords.Add("F_major", new int[] { 65, 69, 72 });

        //GameVariables.Chords.Add("G#_4", new int[] { 68 });
        //GameVariables.Chords.Add("A♭_4", new int[] { 68 });
        //GameVariables.Chords.Add("A_4", new int[] { 69 });
        //GameVariables.Chords.Add("A#_4", new int[] { 70 });
        //GameVariables.Chords.Add("B♭_4", new int[] { 70 });
        //GameVariables.Chords.Add("B_4", new int[] { 71 });

        //GameVariables.Chords.Add("C_5", new int[] { 72 });
        //GameVariables.Chords.Add("C#_5", new int[] { 73 });
        //GameVariables.Chords.Add("D♭_5", new int[] { 73 });
        //GameVariables.Chords.Add("D_5", new int[] { 74 });
        //GameVariables.Chords.Add("D#_5", new int[] { 75 });
        //GameVariables.Chords.Add("E♭_5", new int[] { 75 });
        //GameVariables.Chords.Add("E_5", new int[] { 76 });
        //GameVariables.Chords.Add("F_5", new int[] { 77 });
        //GameVariables.Chords.Add("F#_5", new int[] { 78 });
        //GameVariables.Chords.Add("G♭_5", new int[] { 78 });
        //GameVariables.Chords.Add("G_5", new int[] { 79 });
        //GameVariables.Chords.Add("G#_5", new int[] { 80 });
        //GameVariables.Chords.Add("A♭_5", new int[] { 80 });
        //GameVariables.Chords.Add("A_5", new int[] { 81 });
        //GameVariables.Chords.Add("A#_5", new int[] { 82 });
        //GameVariables.Chords.Add("B♭_5", new int[] { 82 });
        //GameVariables.Chords.Add("B_5", new int[] { 83 });

        //GameVariables.Chords.Add("C_6", new int[] { 84 });
        //GameVariables.Chords.Add("C#_6", new int[] { 85 });
        //GameVariables.Chords.Add("D♭_6", new int[] { 85 });
        //GameVariables.Chords.Add("D_6", new int[] { 86 });
        //GameVariables.Chords.Add("D#_6", new int[] { 87 });
        //GameVariables.Chords.Add("E♭_6", new int[] { 87 });
        //GameVariables.Chords.Add("E_6", new int[] { 88 });
        //GameVariables.Chords.Add("F_6", new int[] { 89 });
        //GameVariables.Chords.Add("F#_6", new int[] { 90 });
        //GameVariables.Chords.Add("G♭_6", new int[] { 90 });
        //GameVariables.Chords.Add("G_6", new int[] { 91 });
        //GameVariables.Chords.Add("G#_6", new int[] { 92 });
        //GameVariables.Chords.Add("A♭_6", new int[] { 92 });
        //GameVariables.Chords.Add("A_6", new int[] { 93 });
        //GameVariables.Chords.Add("A#_6", new int[] { 94 });
        //GameVariables.Chords.Add("B♭_6", new int[] { 94 });
        //GameVariables.Chords.Add("B_6", new int[] { 95 });

        //GameVariables.Chords.Add("C_7", new int[] { 96 });
        //GameVariables.Chords.Add("C#_7", new int[] { 97 });
        //GameVariables.Chords.Add("D♭_7", new int[] { 97 });
        //GameVariables.Chords.Add("D_7", new int[] { 98 });
        //GameVariables.Chords.Add("D#_7", new int[] { 99 });
        //GameVariables.Chords.Add("E♭_7", new int[] { 99 });
        //GameVariables.Chords.Add("E_7", new int[] { 100 });
        //GameVariables.Chords.Add("F_7", new int[] { 101 });
        //GameVariables.Chords.Add("F#_7", new int[] { 102 });
        //GameVariables.Chords.Add("G♭_7", new int[] { 102 });
        //GameVariables.Chords.Add("G_7", new int[] { 103 });
        //GameVariables.Chords.Add("G#_7", new int[] { 104 });
        //GameVariables.Chords.Add("A♭_7", new int[] { 104 });
        //GameVariables.Chords.Add("A_7", new int[] { 105 });
        //GameVariables.Chords.Add("A#_7", new int[] { 106 });
        //GameVariables.Chords.Add("B♭_7", new int[] { 106 });
        //GameVariables.Chords.Add("B_7", new int[] { 107 });

        //GameVariables.Chords.Add("B_8", new int[] { 108 });
    }
}
