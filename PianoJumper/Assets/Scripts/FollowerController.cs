﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowerController : MonoBehaviour
{
    public float offset;
    public GameObject Player;

    void Update()
    {
        transform.position = new Vector3(Player.transform.position.x + offset, 0, 0);
    }

    private void OnEnable()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }
}
