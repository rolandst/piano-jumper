﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine;
using System;

public class KeySpawner : MonoBehaviour
{
    public int keyCount;
    public Camera cam;
    public GameObject WhiteKey;
    public GameObject BlackKey;

    char[] notes = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G' };
    char[] octaves = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8' };
    char[] halftones = new char[] { 'A', 'C', 'D', 'F', 'G' };

    int noteIndex;
    int octaveIndex;
    int midiNumber;

    // Start is called before the first frame update
    void Start()
    {
        midiNumber = 21;
        PlaceKeys();
    }

    void PlaceKeys()
    {
        noteIndex = 0;
        octaveIndex = 0;

        float optimalKeyWidth = (float)Screen.width / (float)keyCount;
        float keyWidth = WhiteKey.GetComponent<RectTransform>().rect.width;
        float keyHeight = WhiteKey.GetComponent<RectTransform>().rect.height;
        float keyScale = optimalKeyWidth / keyWidth;

        for (int i = 0; i < keyCount; i++)
        {
            float posX = (i * optimalKeyWidth) - (float)Screen.width / 2f;
            float posY = 30f;
            GameObject wKey = Instantiate(WhiteKey, transform);
            wKey.GetComponent<KeyControl>().midiNumber = midiNumber++;
            wKey.GetComponent<KeyControl>().keyType = 'w';
            wKey.transform.localScale = new Vector3(keyScale, keyScale, keyScale);
            wKey.transform.localPosition = new Vector3(posX, posY, 0f);
            wKey.transform.SetAsFirstSibling();
            wKey.name = $"WhiteKey_{i}";
            wKey.GetComponentInChildren<Text>().text = $"{notes[noteIndex]}<size=20>{octaves[octaveIndex]}</size>";

            if (halftones.Contains(notes[noteIndex]) && octaves[octaveIndex] != '8')
            {
                posX = ((i * optimalKeyWidth) + (optimalKeyWidth - 0.5f * (keyWidth * (keyScale * (2f / 3f))))) - (float)Screen.width / 2f;
                posY = posY + ((0.5f * keyHeight) * keyScale * (2f / 3f));
                string keyLabel = PutHalftone(notes[noteIndex], octaves[octaveIndex]);
                GameObject bKey = Instantiate(BlackKey, transform);
                bKey.GetComponent<KeyControl>().midiNumber = midiNumber++;
                bKey.GetComponent<KeyControl>().keyType = 'b';
                bKey.transform.localScale = new Vector3(keyScale * (2f / 3f), keyScale * (2f / 3f), keyScale * (2f / 3f));
                bKey.transform.localPosition = new Vector3(posX, posY, 0f);
                bKey.name = $"BlackKey_{i}";
                bKey.GetComponentInChildren<Text>().text = keyLabel;                
            }

            if (notes[noteIndex] == 'B')
            {
                octaveIndex++;
            }            

            if (notes[noteIndex] == 'G')
            {
                noteIndex = 0;
                continue;
            }

            noteIndex++;
        }
    }

    private string PutHalftone(char v, char octave)
    {
        string returnValue;

        switch (v)
        {
            case 'A':
                returnValue = $"A#<size=20>{octave}</size>\n\nB♭<size=20>{octave}</size>";
                break;
            case 'C':
                returnValue = $"C#<size=20>{octave}</size>\n\nD♭<size=20>{octave}</size>";
                break;
            case 'D':
                returnValue = $"D#<size=20>{octave}</size>\n\nE♭<size=20>{octave}</size>";
                break;
            case 'F':
                returnValue = $"F#<size=20>{octave}</size>\n\nG♭<size=20>{octave}</size>";
                break;
            case 'G':
                returnValue = $"G#<size=20>{octave}</size>\n\nA♭<size=20>{octave}</size>";
                break;
            default:
                returnValue = string.Empty;
                break;
        }

        return returnValue;
    }
}
