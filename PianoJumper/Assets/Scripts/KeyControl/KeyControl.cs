﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using MidiJack;
using UnityEngine;

public class KeyControl : MonoBehaviour
{
    [HideInInspector]
    public int midiNumber;

    [HideInInspector]
    public char keyType;

    RawImage keyColor;

    int[] combo;

    Color bk;
    Color wh;
    Color pressed;
    Color topress;

    private void Start()
    {
        keyColor = GetComponent<RawImage>();
        bk = Color.black;
        wh = Color.white;
        pressed = new Color(102f/255, 205f/255, 170f/255);
        topress = new Color(119f/255, 136f/255, 153f/255);
    }

    // Update is called once per frame
    void Update()
    {
        if (MidiMaster.GetKey(midiNumber) > 0.01f)
        {
            keyColor.color = pressed;
        }
        else if (GameVariables.ComboStack.Count > 0)
        {
            HighlightKey();
        }
        else
        {
            SetDefaultKeyColor();
        }
    }

    private void HighlightKey()
    {
        if (GameVariables.ComboStack.ElementAt(0).Contains(midiNumber))
        {
            keyColor.color = topress;
        }
        else
        {
            SetDefaultKeyColor();
        }
    }

    private void SetDefaultKeyColor()
    {
        switch (keyType)
        {
            case 'w':
                keyColor.color = wh;
                break;
            case 'b':
                keyColor.color = bk;
                break;
        }
    }
}
