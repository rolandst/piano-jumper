﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObstacleChangeKey : MonoBehaviour
{
    GameController gc;
    private void Awake()
    {
        gc = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            Debug.Log("player jumped over");
            GameVariables.ComboStack.Remove(GameVariables.ComboStack.ElementAt(0));
            gc.UpdateComboText();
        }
    }
}
