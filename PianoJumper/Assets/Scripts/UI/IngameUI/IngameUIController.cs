﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class IngameUIController : MonoBehaviour
{
    public Text T_BPM;

    private void OnEnable()
    {
        T_BPM.text = $"BPM: {GameVariables.BPM}";
    }
}
