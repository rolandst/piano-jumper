﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public Text txtBPM;
    public Text txtCombo;

    public Slider sliderBPM;

    public Sprite[] heartTextures;

    public GameObject PauseBtn;
    public GameObject StartUI;
    public GameObject IngameUI;
    public GameObject PauseUI;
    public GameObject GameLostUI;
    public GameObject Spawnpoints;
    public GameObject GameController;
    public GameObject KeyBoard;
    public GameObject[] Hearts;

    SpawnController SpawnControl;
    GameController Gc;

    private void Start()
    {
        OnBPMChanged(sliderBPM);
        SpawnControl = Spawnpoints.GetComponent<SpawnController>();
        Gc = GameController.GetComponent<GameController>();
    }

    private void SetBPM(int bpm)
    {
        GameVariables.BPM = bpm;
        txtBPM.text = $"BPM: {bpm}";
    }

    public void OnBPMChanged(Slider slider)
    {
        SetBPM((int)slider.value);
    }

    public void OnStart()
    {
        StartUI.SetActive(false);
        KeyBoard.SetActive(true);
        IngameUI.SetActive(true);
        SpawnControl.SpawnPlayer();
        Gc.EnableObstacleSpawner();
        GameVariables.OverpassedObstacleCount = 0;
        Gc.UpdateObstacleCount();
        GameVariables.GAMEON = true;
    }

    public void OnPause()
    {
        PauseBtn.SetActive(false);
        KeyBoard.SetActive(false);
        PauseUI.SetActive(true);
        GameVariables.GAMEPAUSED = true;
    }

    public void OnResume()
    {
        KeyBoard.SetActive(true);
        PauseBtn.SetActive(true);
        PauseUI.SetActive(false);
        GameVariables.GAMEPAUSED = false;
    }

    public void OnRestart()
    {
        StartCoroutine(Gc.DestrtoyAllObstacles());
        GameLostUI.SetActive(false);
        OnStart();
    }

    public void OnGameLost()
    {
        GameVariables.GAMEON = false;
        Gc.DisableObstacleSpawner();
        KeyBoard.SetActive(false);
        PauseBtn.SetActive(false);
        GameLostUI.SetActive(true);
    }

    public void OnExit()
    {
        GameVariables.GAMEON = false;
        GameVariables.GAMEPAUSED = false;
        GameVariables.ComboNameStack.Clear();
        GameVariables.ComboStack.Clear();

        txtCombo.text = string.Empty;

        Gc.DestroyPlayer();
        StartCoroutine(Gc.DestrtoyAllObstacles());
        Gc.DisableObstacleSpawner();

        KeyBoard.SetActive(false);
        IngameUI.SetActive(false);
        PauseBtn.SetActive(true);
        PauseUI.SetActive(false);
        GameLostUI.SetActive(false);
        StartUI.SetActive(true);

        StartCoroutine(UpdateHearts());
    }

    public IEnumerator UpdateHearts()
    {
        int lives = GameVariables.LIVECOUNT;

        if (lives > -1)
        {
            foreach (GameObject Heart in Hearts)
            {
                if (lives > 0)
                    Heart.GetComponent<Image>().sprite = heartTextures[1];
                else
                    Heart.GetComponent<Image>().sprite = heartTextures[0];

                lives--;

                yield return null;
            }
        }        
    }
}
